
# Example graphql

## Query course

```javascript
- Example 1
query getSingleCourse($courseId: Int!) {
  course(id: $courseId) {
    title
    author
    description,
    topic
  }
}
const variable = {
  "courseId": 1
}

- Example 2
query {
    course(id: 1) {
        topic
        description
    }
}
```

## Query courses

```javascript
query getCourses($topic: String) {
  courses(topic: $topic) {
    title
    author
    description,
    topic
  }
}

const variable = {
  "topic": "Node.js"
}
```

## Query course with fragment

```javascript
query getCoursesWithFragments($courseId1: Int, $courseId2: Int) {
  course1: course(id: $courseId1) {
    ... courseFields
  }

  course2: course(id: $courseId2) {
    ... courseFields
  }
}

fragment courseFields on Course {
  title
  author
  description,
  topic
}
```

## Mutation update

```javascript
- Example 1

mutation updateCourseTopic($id: Int!, $topic: String! ) {
  updateCourseTopic(id: $id, topic: $topic) {
    ... courseFields  
  }
}

fragment courseFields on Course {
  title
  author
  description,
  topic
}

- Example 2

mutation {
    updateCourseTopic(id: 1, topic: "change topic") {
      title 
      author
      description
      topic
    }
}

```
