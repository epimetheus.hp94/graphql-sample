// const gql = require('graphql-tag');


// module.exports = gql(`
//     {
//         run: String
//     }
// `);


const { buildSchema } = require('graphql');


// GraphQL Schema

const schema = buildSchema(`
    type Query {
        course(id: Int!): Course
        courses(topic: String): [Course]
        message: String,
    }
    type Mutation {
        updateCourseTopic(id: Int!, topic: String!): Course
    }
    type Course {
        id: Int
        title: String
        author: String
        description: String
        topic: String
        url: String
    }
`);


module.exports = {
    schema
}