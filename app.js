const express = require('express');
const graphqlHTTP = require('express-graphql');
const { buildSchema } = require('graphql');
const {schema} = require('./schema/schema');
const {coursesData} = require('./sample-data/sample');
// Root resolver
const root = {
  message: () => "Type query message function the nay dc chua"
};


module.exports =  () => {
    const app = express();

    app.use(
      '/graphql',
      graphqlHTTP((request, response, graphQLParams) => (
        {
        schema: schema,
        rootValue: root,
        graphiql: true,
        // context: {container: console.log(response)}
      })),
    );

    return app;
}

// Create an expres server and a GraphQL endpoint



