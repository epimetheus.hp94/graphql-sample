const express = require('express');
const graphqlHTTP = require('express-graphql');
const { buildSchema } = require('graphql');
const {schema} = require('./schema/schema');
const {coursesData} = require('./sample-data/sample');

// const schema = require('./course/graphql/typedef');

const getCourse = function (args) {
  const id = 1;
  return coursesData.filter(course => {
    return course.id == id;
  })[0];
}

const getCourses = function (args) {
  if (args.topic) {
    const topic = args.topic;
    return coursesData.filter(course => course.topic === topic);
  } else {
    return coursesData;
  }
}

const updateCourseTopic = function ({ id, topic }) {
  coursesData.map(course => {
    if (course.id === id) {
      course.topic = topic;
      return course;
    }
  });
  return coursesData.filter(course => course.id === id)[0];
}

// Root resolver
const root = {
  course: getCourse,
  courses: getCourses,
  updateCourseTopic: updateCourseTopic,
  message: () => "Type query message function"
};


// Create an expres server and a GraphQL endpoint
const app = express();
// app.use('/graphql', graphqlHTTP({
//   schema: schema,
//   rootValue: root,
//   graphiql: true
// }));

app.use(
  '/graphql',
  graphqlHTTP((request, response, graphQLParams) => (
    {
    schema: schema,
    rootValue: root,
    graphiql: true,
    context: {container: console.log(response)}
  })),
);

app.listen(3001, () => console.log('Express GraphQL Server Now Running On localhost:3001/graphql'));