const { asClass, asValue, asFunction } = require('awilix');
const container = require('./container');
const config = require('./config');

/**
 * ensures all essential modules are already available before lunching app.
 * @returns {Promise<container>}
 */
module.exports = async () => {
  container.register({
    config: asValue(config),
  });
  return container;
};


